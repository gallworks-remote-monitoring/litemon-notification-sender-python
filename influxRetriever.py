import os # For the environmental variable
import typing # Used for returning a List
import influxdb_client, time
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
import litemonLogger

class influxDataRetriever:
    def __init__(self, InfluxBucket = "LiteMon"):
        self.url = os.environ.get("INFLUXDB_URL")
        self.token = os.environ.get("INFLUXDB_TOKEN")
        self.org = os.environ.get("INFLUXDB_ORG")
        self.client = influxdb_client.InfluxDBClient(url=self.url, token=self.token, org=self.org)
        self.InfluxBucket = InfluxBucket
        self.query_api = self.client.query_api()
        self.logger = litemonLogger.setupLogging("Influx")

    def sendQuery(self, installLocation: str, field: str, device: str, installAddress: str):
        query = 'from(bucket: "' + self.InfluxBucket + '")\n'
        query += '|> range(start: -5m)\n'
        query += '|> filter(fn: (r) => r._measurement == "Temp")\n'
        query += '|> filter(fn: (r) => r.Location == "' + installLocation + '")\n'
        query += '|> filter(fn: (r) => r.Address == "' + installAddress + '")\n'
        query += '|> filter(fn: (r) => r.Device == "' + device + '")\n'
        query += '|> filter(fn: (r) => r._field == "' + field + '")'
        self.logger.debug("Assembled Query: " + query)
        tables = self.query_api.query(query=query, org=self.org)
        return tables