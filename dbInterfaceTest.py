import litemonDatabase
import litemonLogger
import sqlite3

def clientTable_Test():
    logger = litemonLogger.setupLogging("Clinet Test")
    clientData = litemonDatabase.ClientTableInterface()
    con = sqlite3.connect("litemon-client-a.db")
    cur = con.cursor()
    clientData.set_database_cur(cur)
    logger.debug("Litemon DB Interface Initialized")
    # Try to initialize the table.
    clientData.initTable()
    logger.debug("Table Initialized")
    # Try to add clients

    clientOne = litemonDatabase.LiteMonClient(clientID = "9998", lastName = "Doe", firstName = "John", \
                                             primaryPhone="+18005009999", email = "test@test.ca")
    clientTwo = litemonDatabase.LiteMonClient(clientID = "9999", lastName = "Froe", firstName = "Jane", \
                                             primaryPhone="+18005001111", email = "jest@jest.ca")
    clientData.addClient(clientOne)
    clientData.addClient(clientTwo)
    logger.debug("Clients Added")
    # Try to poll the table for data
    clients = clientData.getAll()
    logger.debug("All Clients Retrieved: " + str(len(clients)))
    for client in clients:
        print(client)
    # try to delete client 1
    logger.debug("Removing Client 9998")
    clientData.removeClient("9998")
    clients = clientData.getAll()
    logger.debug("All Clients Retrieved: " + str(len(clients)))
    for client in clients:
        print(client)

    # try to delete client 2
    logger.debug("Removing Client 9999")
    clientData.removeClient("9999")
    clients = clientData.getAll()
    logger.debug("All Clients Retrieved: " + str(len(clients)))
    for client in clients:
        print(client)
    # Commit and close database!
    con.commit()
    con.close()

def installTable_Test():
    logger = litemonLogger.setupLogging("Install Test")
    installData = litemonDatabase.InstallTableInterface()
    con = sqlite3.connect("litemon-client-a.db")
    cur = con.cursor()
    installData.set_database_cur(cur)
    logger.debug("Litemon DB Interface Initialized")
    # Attempt to initiate the table
    installData.initTable()
    logger.debug("Table initialized")
    # Create some Installs
    installOne = litemonDatabase.LiteMonInstall("9999", "9998", "10Universal", "Inuvik", "basic")
    installData.addInstall(installOne)
    allInstalls = installData.getAll()
    for install in allInstalls:
        print(install)
    logger.debug("Attempting to remove install")
    installData.removeInstall("9999")
    allInstalls = installData.getAll()
    for install in allInstalls:
        print(install)
    logger.debug("Done")

def limitTable_Test():
    logger = litemonLogger.setupLogging("Limit Test")
    limitData = litemonDatabase.LimitTableInterface()
    con = sqlite3.connect("litemon-client-a.db")
    cur = con.cursor()
    limitData.set_database_cur(cur)
    logger.debug("Litemon DB Interface Initialized")
    # Attempt to initiate the table
    limitData.initTable()
    logger.debug("Table Initialized")
    # Create some limits
    limitOne = litemonDatabase.LiteMonLimit("9999","9998", "A", "5","100")
    limitData.addLimit(limitOne)
    allLimits = limitData.getAll()
    for limit in allLimits:
        print(limit)
    logger.debug("Attemptng to remove limit")
    limitData.removeLimit("9999")
    allLimits = limitData.getAll()
    for limit in allLimits:
        print(limit)
    logger.debug("Done")

def main():
    clientTable_Test()
    installTable_Test()
    limitTable_Test()


if __name__ == "__main__":
    main()