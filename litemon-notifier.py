import sqlite3

from twilio.rest import Client
import os # Needed for environment variable access.
import sys
from influxRetriever import influxDataRetriever
import litemonLogger
import litemonDatabase


class twilMessanger:
    '''Class to abstract the twilio messanger needs. Also adds to the log file with a unique name'''
    def __init__(self, account_sid, auth_token, phone_number):
        self.client = Client(account_sid,auth_token)
        self.logger = litemonLogger.setupLogging("twil")
        self.phone = phone_number


    def sendAlert(self, phoneNumber: str, message: str):
        self.logger.info("Sending Message to " + str(phoneNumber))
        self.logger.debug("Message: " + str(message))
        message = self.client.messages \
            .create(
                body= message,
                from_=self.phone,
                to = phoneNumber
            )
        self.logger.debug("Sent: " + message.sid)

def main():    
    pal = litemonLogger.setupLogging(__name__)
    pal.debug("LITEMON NOTIFIER RUNNING")
    pal.debug("Current Working Directory: " + str(os.getcwd()))
    pal.debug("Setting Up Influx Client...")
    dataRetriever = influxDataRetriever()

    # Find your Account SID and Auth Token at twilio.com/console
    # and set the environment variables. See http://twil.io/secure


    account_sid = os.environ.get("TWILIO_ACCOUNT_SID")
    auth_token = os.environ.get("TWILIO_AUTH_TOKEN")
    phone_number = os.environ.get("TWILIO_PHONE_NUMBER")
    pal.debug("Setting Up Twilio Client...")
    twilioClient = twilMessanger(account_sid, auth_token, phone_number)

    # Create a connection with the DB
    pal.debug("Connecting to SQLite Database...")
    con = sqlite3.connect("/home/pi/Documents/litemon-notification-sender-python/litemon-client-a.db")
    cur = con.cursor()

    # Install Table Interface
    clientInterface = litemonDatabase.ClientTableInterface()
    clientInterface.set_database_cur(cur)
    installInterface = litemonDatabase.InstallTableInterface()
    installInterface.set_database_cur(cur)
    limitInterface = litemonDatabase.LimitTableInterface()
    limitInterface.set_database_cur(cur)

    # Get the list of Install Locations
    pal.debug("Getting Install List From Database")
    allInstalls = installInterface.getAll()
    pal.debug("Installs Found: " + str(len(allInstalls)))
    for install in allInstalls:
        decodedInstall = installInterface.decodeInstallTuple(install)
        # Check each install!
        pal.debug("Selecting Install: " + str(install))
        pal.debug("Sending InfluxDB Query")
        # Get a list of limits
        allLimits = limitInterface.getLimitsOnInstall(install[0])
        for limit in allLimits:
            decodedLimit = limitInterface.decodeLimitTuple(limit)
            pal.debug("Secting Limit: " + str(limit))
            tables = dataRetriever.sendQuery(installLocation=decodedInstall.location, field=decodedLimit.channelName, device = decodedInstall.device, installAddress= decodedInstall.streetAddress)
            if len(tables) > 0:
                try:
                    pal.debug("Ch: " + str(limit[2]) + " = " + str(round(tables[0].records[0].get_value(),2)))
                    channelValue = tables[0].records[0].get_value()
                except:
                    pal.error("Problem Reading Influx Return")
                else:
                    # Process the limits
                    sendAlert = False
                    alertMessage = "LITEMON ALERT\n"
                    alertMessage += "Install: " + str(install[2]) + "\n"
                    if(channelValue < int(limit[3])) :
                        pal.warning("Value for "  + str(limit[2]) + " Below Min Safe Value")
                        alertMessage += str(limit[2]) + " Below Min Safe Value"
                        sendAlert = True
                    elif(channelValue > int(limit[4])) :
                        pal.warning("Value for " + str(limit[2]) + " Exceeding Max Safe Value")
                        alertMessage += str(limit[2]) + " Exceeding Max Safe Value"
                        sendAlert = True
                    if(sendAlert):
                        # Get Customer Info
                        clientID = installInterface.getClientIDFromInstallID(install[0])
                        contact_number = clientInterface.getPrimaryPhoneFromClientID(clientID[0][0])
                        twilioClient.sendAlert(contact_number[0], alertMessage)
            else: # Len(tables) =< 0
                pal.warning("No data in Influx Return, could be error, or dead node: " + str(tables))


if __name__ == "__main__":
    main()
