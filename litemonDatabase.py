# Initializes the Client Database - ONLY RUN ONCE!

## USAGE
# - Run this to create the DB on a fresh install of the code. 
# - Run this ahead of a migration to create the new DB after adjusting the script.

import sqlite3
from os.path import exists
from dataclasses import dataclass # For Dataclass definitions
from typing import Tuple # for returning tuples
import litemonLogger

class LiteMonDBInterface:
    '''A super class that contains the common elements of all the db interfaces, extended to suit the specific table'''
    def __init__(self, tableName:str, cols: str):
        self.table_name = tableName
        self.cols = cols
        self.logger = litemonLogger.setupLogging(self.table_name + "-db")
        self.logger.debug(self.table_name + " DB Interface Initialized")

    def set_database_cur(self, cursor: sqlite3.Cursor):
        self.cur = cursor

    def initTable(self):
        # Get database connection
        try:
            self.cur.execute("CREATE TABLE " + self.table_name + self.cols)
            self.logger.debug("Initalized " + self.table_name + " Table")
        except Exception as e:
            print(e)    

    def getAll(self):
        self.cur.execute("SELECT * from " + self.table_name)
        self.logger.debug("Gathering all from DB")
        return self.cur.fetchall()

@dataclass
class LiteMonClient:
    """Class for storing information about each client who has a litemon node installed"""
    clientID: str = "Blank"
    lastName: str = "Blank"
    firstName: str = "Blank"
    email: str = "apps@gallworks.ca"
    primaryPhone: str  = "n/a" # Might want to do a number?
    secondaryPhone: str = "n/a" # Can be null, or empty (n/a)
    

class ClientTableInterface(LiteMonDBInterface):
    def __init__(self):
        LiteMonDBInterface.__init__(self,tableName ="client", cols = "(client_id, last_name, first_name, primary_phone, secondary_phone, primary_email)")

    def addClient(self, newClient: LiteMonClient):
        command = '''INSERT INTO client(client_id, last_name, first_name, primary_phone, secondary_phone, primary_email) VALUES(?,?,?,?,?,?)'''
        self.logger.debug("Prepping Command: " + command)
        data = (newClient.clientID, newClient.lastName, newClient.firstName, newClient.primaryPhone, newClient.secondaryPhone, newClient.email)
        self.logger.debug("Prepping Data" + str(data))
        self.cur.execute(command,data)
        self.cur.connection.commit()
        self.logger.debug("Added new client: " + newClient.firstName + " " + newClient.lastName)

    def removeClient(self, clientID: str):
        command = '''DELETE from client WHERE client_id = ?'''
        data = (clientID,)
        self.cur.execute(command,data)
        self.cur.connection.commit()
        self.logger.debug("Client ID " + clientID + " deleted")

    def getPrimaryPhoneFromClientID(self, clientID: str):
        command = '''SELECT primary_phone from client WHERE client_id = ?'''
        self.logger.debug("Prepping Command: " + command)
        data = (clientID,)
        self.logger.debug("Prepping Data: " + str(data))
        self.cur.execute(command, data)
        return self.cur.fetchall()




@dataclass
class LiteMonInstall:
    """Class for storing information about each litemon installation location
        There can be multiple litemon installs per client"""
    installID: str = "Blank"
    clientID: str = "Blank"
    device: str = "Blank"
    streetAddress: str = "None"
    location: str = "None"
    community: str = "None"
    serviceType: str = "basic" # defaults to basic

class InstallTableInterface(LiteMonDBInterface):
    '''A class that provides add and remove functionality to the install table of the litemonDB'''
    def __init__(self):
        LiteMonDBInterface.__init__(self,tableName="install", cols="(install_id, client_id, street_address, location, community, service_type)")

    def addInstall(self, newInstall: LiteMonInstall):
        command = '''INSERT INTO install(install_id, client_id, device, street_address, location, community, service_type) VALUES(?,?,?,?,?,?,?)'''
        self.logger.debug("Prepping Command: " + command)
        data = (newInstall.installID, newInstall.clientID, newInstall.device, newInstall.streetAddress, newInstall.location, newInstall.community, newInstall.serviceType)
        self.logger.debug("Prepping Data")
        self.cur.execute(command,data)
        self.cur.connection.commit()
        self.logger.debug("Added new install: " + newInstall.installID + " at " + newInstall.streetAddress + " and " + newInstall.location)

    def removeInstall(self, installID: str):
        command = '''DELETE from install WHERE install_id = ?'''
        data = (installID,)
        self.cur.execute(command,data)
        self.cur.connection.commit()
        self.logger.debug("Install ID " + installID + " deleted")
    
    def getClientIDFromInstallID(self, installID: str):
        command = '''SELECT client_id from install WHERE install_id = ?'''
        self.logger.debug("Prepping Command: " + command)
        data = (installID,)
        self.logger.debug("Prepping Data: " + str(data))
        self.cur.execute(command, data)
        return self.cur.fetchall()
    
    def decodeInstallTuple(self, installData: Tuple[str,]) -> LiteMonInstall:
        install = LiteMonInstall()
        install.installID = installData[0]
        install.clientID = installData[1]
        install.device = installData[2]
        install.streetAddress = installData[3]
        install.location = installData[4]
        install.community = installData[5]
        install.serviceType = installData[6]
        return install



@dataclass
class LiteMonLimit:
    """Class for storing what the limits of a specific channel
        There can be multiple limits per litemon install"""
    limitID: str = "Blank"
    installID: str = "Blank"
    channelName: str = "A"
    minVal: float = 0.0
    maxVal: float = 0.0

class LimitTableInterface(LiteMonDBInterface):
    '''A class that provides add and remove functionality to the limit table of the litemonDB'''
    def __init__(self):
        LiteMonDBInterface.__init__(self, tableName = "limits", cols = "(limit_id, install_id, channel_name, min_val, max_val)")

    def addLimit(self, newLimit: LiteMonLimit):
        command = '''INSERT INTO limits(limit_id, install_id, channel_name, min_val, max_val) VALUES(?,?,?,?,?)'''
        self.logger.debug("Prepping Command: " + command)
        data = (newLimit.limitID, newLimit.installID, newLimit.channelName, newLimit.minVal, newLimit.maxVal)
        self.logger.debug("Prepping Data: " + str(data))
        self.cur.execute(command,data)
        self.cur.connection.commit()
        self.logger.debug("Added new limit: " + newLimit.limitID + " " + newLimit.channelName)

    def removeLimit(self, limitID: str):
        command = '''DELETE from limits WHERE limit_id = ?'''
        data = (limitID,)
        self.cur.execute(command,data)
        self.cur.connection.commit()
        self.logger.debug("Limit ID " + limitID + " deleted")

    def getLimitsOnInstall(self, installID: str):
        command = '''SELECT * from limits WHERE install_id = ?'''
        self.logger.debug("Prepping Command: " + command)
        data = (installID,)
        self.logger.debug("Prepping Data: " + str(data))
        self.cur.execute(command, data)
        return self.cur.fetchall()
    
    def decodeLimitTuple(self, limitData: Tuple[str,]) -> LiteMonLimit:
        limit = LiteMonLimit()
        limit.limitID = limitData[0]
        limit.installID = limitData[1]
        limit.channelName = limitData[2]
        limit.minVal = limitData[3]
        limit.maxVal = limitData[4]
        return limit

class databaseConnection:
    def __init__(self):
        self.dbName = "litemon-client-a.db" # First Version
        self.con = sqlite3.connect(self.dbName)
        self.cur = self.con.cursor()

    def getCursor(self):
        pass

    def getInstallationList():
        res = self.cur.execute("SELECT ")

    # Returns the channel a and b limits in format (AMax, AMin, BMax, BMin)
    def GetLimits(client: str) -> LiteMonLimit :
        result = LiteMonLimit(clientID = client)
        #search database for the clientID
        
        pass

def main():
    print("Creating Basic Empty Database...")
    # DB Name
    dbName = "litemon-client-a.db" # First Version
    # Check if the database already exists
    if (exists(dbName)):
        print("DB Already Exists, Exiting...")
        exit()
    # CREATE DB
    con = sqlite3.connect(dbName)
    cur = con.cursor()
    # CREATE TABLES
    print("Creating Tables for DB: " + dbName)
    ## Client Table
    cur.execute("CREATE TABLE client(client_id, last_name, first_name, primary_phone, secondary_phone, primary_email)")
    ## Install Table
    cur.execute("CREATE TABLE install(install_id, client_id, device, street_address, location, community, service_type)")
    ## Limit Table
    cur.execute("CREATE TABLE limits(limit_id, install_id, channel_name, min_val, max_val)")
    con.close()
    print("Done.")

if __name__ == "__main__":
    main()
