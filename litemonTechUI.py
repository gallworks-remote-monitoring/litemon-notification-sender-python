# Adding a Client's Info to the client database!

## Usage
# - It's interactive! Go through the prompts, confirm at the end, bingo, added!

import sqlite3
from dataclasses import dataclass
import litemonDatabase
import litemonLogger
import hashlib # for creating client IDs


# Main Helpers
def processYesNoQuestion(message: str)  -> bool:
    response = input(message)
    if(response == 'y'):
        return True
    else:
        return False

def processOptionalQuestion(message: str, optionalParameter: str) -> str:
    return "No"

def addClientFromTerm():
    print(" -- ADD CLIENT -- ")
    print(" -- CLIENT INFO -- ")
    newClient = litemonDatabase.LiteMonClient()
    newClient.lastName = input("Enter Client's Last Name: ")
    newClient.firstName = input("Enter Client's First Name: ")
    newClient.primaryPhone = input("Enter Client's Primary Phone: ")
    newClient.secondaryPhone = input("Enter Client's Secondary Phone (Optional): ")
    newClient.email = input("Enter Client's Email Address: ")
    # WRITE TO DB
    pal = litemonLogger.setupLogging("client-adder")
    dbConnect = litemonDatabase.databaseConnection()
    dbInterface = litemonDatabase.ClientTableInterface()
    dbInterface.set_database_cur(dbConnect.cur)
    pal.debug("DB Connected")
    newClient.clientID = hashlib.sha1(str(newClient.firstName + newClient.lastName + newClient.primaryPhone).encode("UTF-8")).hexdigest()
    pal.debug("ClientID Generated: " + newClient.clientID)
    dbInterface.addClient(newClient)
    #dbConnect.con.commit() # TODO Restructure to not need this!!
    pal.info("Client Added: " + str(newClient))

def removeClient():
    print(" -- REMOVE CLIENT -- ")
    pal = litemonLogger.setupLogging("client-remover")
    dbConnect = litemonDatabase.databaseConnection()
    ClientInterface = litemonDatabase.ClientTableInterface()
    ClientInterface.set_database_cur(dbConnect.cur)
    allClients = ClientInterface.getAll()
    print("--- CLIENT LIST ------------------------------------------------------------------------")
    count = 0
    for client in allClients:
        print(str(count) + ": " + str(client))
        count = count + 1
    print("----------------------------------------------------------------------------------------")
    clientIndex = int(input("Please enter the number of the client to remove: "))
    clientID = allClients[clientIndex][0]
    ClientInterface.removeClient(clientID=clientID)
    pal.info("Client Removed: " + str(clientID))


def addInstallFromTerm():
    print(" -- ADD INSTALL -- ")
    print(" -- INSTALL INFO -- ")
    pal = litemonLogger.setupLogging("install-adder")
    newInstall = litemonDatabase.LiteMonInstall()
    # Which Client is this install associated with?
    dbConnect = litemonDatabase.databaseConnection()
    ClientInterface = litemonDatabase.ClientTableInterface()
    ClientInterface.set_database_cur(dbConnect.cur)
    allClients = ClientInterface.getAll()
    print("--- CLIENT LIST ------------------------------------------------------------------------")
    count = 0
    for client in allClients:
        print(str(count) + ": " + str(client))
        count = count + 1
    print("----------------------------------------------------------------------------------------")
    clientIndex = int(input("Please enter the number of the client associated with this install: "))
    newInstall.clientID = allClients[clientIndex][0]
    print("Adding install to client: " + allClients[clientIndex][2] + " " + allClients[clientIndex][1])
    print("----------------------------------------------------------------------------------------")
    print(" -- LOCATION INFO -- ")
    newInstall.device = input("Enter the device being used: " )
    newInstall.streetAddress = input("Enter Installation Street Address: ")
    newInstall.location = input("Enter Install Location: ")
    newInstall.community = input("Enter Install Community: ")
    print(" -- SERVICE INFO -- ")
    newInstall.serviceType = input("Enter Package: (basic or custom): ")
    # Generate Hash
    newInstall.installID = hashlib.sha1(str(newInstall.streetAddress + newInstall.community + newInstall.serviceType).encode("UTF-8")).hexdigest()
    print("Summary of Install: ")
    print(str(newInstall))

    # Write to DB
    InstallInterface = litemonDatabase.InstallTableInterface()
    InstallInterface.set_database_cur(dbConnect.cur)
    InstallInterface.addInstall(newInstall)
    pal.info("Install Added: " + str(newInstall))
    
def removeInstall():
    print(" -- REMOVE CLIENT -- ")
    pal = litemonLogger.setupLogging("install-remover")
    dbConnect = litemonDatabase.databaseConnection()
    InstallInterface = litemonDatabase.InstallTableInterface()
    InstallInterface.set_database_cur(dbConnect.cur)
    allInstalls = InstallInterface.getAll()
    print("--- Install LIST ------------------------------------------------------------------------")
    count = 0
    for install in allInstalls:
        print(str(count) + ": " + str(install))
        count = count + 1
    print("----------------------------------------------------------------------------------------")
    installIndex = int(input("Please enter the number of the install to remove: "))
    installID = allInstalls[installIndex][0]
    InstallInterface.removeInstall(installID=installID)
    pal.info("Install Removed: " + str(installID))


def addLimitFromTerm():
    print(" -- ADD LIMIT --")
    
    pal = litemonLogger.setupLogging("limit-adder")
    newLimit = litemonDatabase.LiteMonLimit()
    # Which Install is this install associated with?
    dbConnect = litemonDatabase.databaseConnection()
    InstallInterface = litemonDatabase.InstallTableInterface()
    InstallInterface.set_database_cur(dbConnect.cur)
    allInstalls = InstallInterface.getAll()
    print("--- INSTALL LIST ------------------------------------------------------------------------")
    count = 0
    for install in allInstalls:
        print(str(count) + ": " + str(install))
        count = count + 1
    print("----------------------------------------------------------------------------------------")
    installIndex = int(input("Please enter the number of the install associated with this limit: "))
    newLimit.installID = allInstalls[installIndex][0]
    print(" -- LIMIT INFO -- ")
    newLimit.channelName = input("Enter the Data Channel Name: ") # Trucated down to create the influx location.
    newLimit.minVal = input("Please Enter the Min Safe Value: ")
    newLimit.maxVal = input("Please Enter the Max Safe Value: ")
    newLimit.limitID = hashlib.sha1(str(newLimit.channelName + newLimit.minVal + newLimit.maxVal).encode("UTF-8")).hexdigest()
    print("Summary of Limit: ")
    print(str(newLimit))

    # Write to DB
    limitInterface = litemonDatabase.LimitTableInterface()
    limitInterface.set_database_cur(dbConnect.cur)
    limitInterface.addLimit(newLimit)
    pal.info("Limit Added: " + str(newLimit))

def removeLimit():
    print(" -- REMOVE LIMIT -- ")
    pal = litemonLogger.setupLogging("limit-remover")
    dbConnect = litemonDatabase.databaseConnection()
    limitInterface = litemonDatabase.LimitTableInterface()
    limitInterface.set_database_cur(dbConnect.cur)
    allLimits = limitInterface.getAll()
    print("--- LIMIT LIST ------------------------------------------------------------------------")
    count = 0
    for limit in allLimits:
        print(str(count) + ": " + str(limit))
        count = count + 1
    print("----------------------------------------------------------------------------------------")
    limitIndex = int(input("Please enter the number of the limit to remove: "))
    limitID = allLimits[limitIndex][0]
    limitInterface.removeLimit(limitID=limitID)
    pal.info("Limit Removed: " + str(limitID))

def main():
    while True:
        # Print Main Menu
        print(" -- MAIN MENU --")
        print("1) Add Client")
        print("2) Remove Client")
        print("3) Add Install")
        print("4) Remove Install")
        print("5) Add Limit")
        print("6) Remove Limit")
        print("x) Exit")
        selection = input("Please enter desired operation: ")
        print()

        if(selection == '1'): # Add Client
            addClientFromTerm()
        elif(selection == '2'): # Remove Client
            removeClient()
        elif(selection == '3'): # Add Install
            addInstallFromTerm()
        elif(selection == '4'): # Remove Install
            removeInstall()
        elif(selection == '5'): # Add Limit
            addLimitFromTerm()
        elif(selection == '6'): # Remove Limit
            removeLimit()
        elif(selection == 'x'):
            exit()


if __name__ == "__main__":
    main()
