# LiteMon Notification Sender Python

## Description

The Litemon Notification Sender works to send notifications to users based on values reported to an InfluxDB instance.

## Installation

TBD
## Usage

### Cron Add:

- Note: A environment setup scrip is needed to start the python script correctly!

```* * * * * /home/patrick/setenv.sh python3 /mnt/c/dev/LiteMon-Tools/litemon-notification-sender-python/litemon-notifier.py >> /home/patrick/cron_output.log 2>&1```

## Support
patrick@gallworks.ca

# Database Contents

## v0.1.0 onwards
- The database has three tables: client, install, limits.
- The client table contains:
  - client_id:
  - last_name:
  - first_name:
  - email:
  - primary_phone:
  - secondary_phone:
- The install table contains
  - install_id:
  - client_id:
  - device:
  - street_address:
  - location:
  - community:
  - service_type:
- The limits table contains:
  - limit_id:
  - install_id:
  - channel_name:
  - min_val:
  - max_val:

## Database Interface
Each table has a table interface class. This class inherits a general db interface class
which contains all the common elements needed for interfacing with the SQLite3 DB.

# Road Map

## v0.2.0
- [ ] Install Script that guides user through setting the environment variables
- [ ] Email alerts to customers
- [ ] More notification preferences, intervals between notifications, weekly updates, etc.
- [ ] SQL support as well as SQLite


## License
TBD
## Project status
Under development! See the roadmap above
